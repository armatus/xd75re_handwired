#include QMK_KEYBOARD_H
#include "version.h"
#include "passwords.h"

#define KC_CPY LCTL (KC_INS)
#define KC_PST LSFT (KC_INS)
#define KC_SAV LCTL (KC_S)
#define KC_SHSP MT (MOD_LSFT, KC_SPC)

/* temp  variable for any-control and any-alt */
uint8_t old_rlmk;

enum custom_keycodes {
  VRSN = SAFE_RANGE,
  RUSLAT,
  ANY_CTRL,
  ANY_ALT,
  VORT00
};

enum layers {
  COLEMAK = 0,
  QWERTY,
  RULEMAK,
  UPPER,
  VORTOJ,
  MOUSE
};

const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {
/* Keymap 0: COLEMAK
 *
 * ,------------------------------------------------------------------------------------------------.
 * |   =   |  1  |  2  |  3  |  4  |  5  | ESC  | KANA |ruslat|  6  |  7  |  8  |  9  |  0  |   -   |
 * |-------+-----+-----+-----+-----+-----+------+------+------+-----+-----+-----+-----+-----+-------|
 * |   [   |  Q  |  W  |  F  |  P  |  G  | copy |A.S.] | vrtj |  J  |  L  |  U  |  Y  |  ;  |   ]   |
 * |-------+-----+-----+-----+-----+-----+------+------+------|-----+-----+-----+-----+-----+-------|
 * |  TAB  |  A  |  R  |  S  |  T  |  D  | paste| HOME | INS  |  H  |  N  |  E  |  I  |  O  |   '   |
 * |-------+-----+-----+-----+-----+-----+------+------+------|-----+-----+-----+-----+-----+-------|
 * |  LSPO |  Z  |  X  |  C  |  V  |  B  | gui  | END  | DEL  |  K  |  M  |  :  |  \  |  /  |  RSPC |
 * |-------+-----+-----+-----+-----+-----+------+-------------+-----+-----+-----+-----+-----+-------|
 * | *CTRL |  `  |*ALT |  ,  |  .  |BSPC | SHSP | SHSP | SHSP |Enter|  FN |left |Down | up  | right |
 * `------------------------------------------------------------------------------------------------'
 */	
	[COLEMAK] = LAYOUT(
		KC_EQL,   KC_1,   KC_2,    KC_3,    KC_4,   KC_5,    KC_ESC,  KC_LNG3,       RUSLAT,     KC_6,   KC_7,      KC_8,    KC_9,    KC_0,    KC_MINS,
		KC_LBRC,  KC_Q,   KC_W,    KC_F,    KC_P,   KC_G,    KC_CPY,  A(S(KC_RBRC)), TG(VORTOJ), KC_J,   KC_L,      KC_U,    KC_Y,    KC_SCLN, KC_RBRC,
		KC_TAB,   KC_A,   KC_R,    KC_S,    KC_T,   KC_D,    KC_PST,  KC_HOME,       KC_INS,     KC_H,   KC_N,      KC_E,    KC_I,    KC_O,    LT(MOUSE, KC_QUOT),
		SC_LSPO,  KC_Z,   KC_X,    KC_C,    KC_V,   KC_B,    KC_LGUI, KC_END,        KC_DEL,     KC_K,   KC_M,      KC_COLN, KC_BSLS, KC_SLSH, SC_RSPC,
		ANY_CTRL, KC_GRV, ANY_ALT, KC_COMM, KC_DOT, KC_BSPC, KC_SHSP, KC_SHSP,       KC_SHSP,    KC_ENT, MO(UPPER), KC_LEFT, KC_DOWN, KC_UP,   KC_RGHT
	),
/* Keymap 1: QWERTY
 *
 * ,-----------------------------------------------------------------------------------------.
 * |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |
 * |-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----|
 * |     |  Q  |  W  |  E  |  R  |  T  |     |     |     |  Y  |  U  |  I  |  O  |  P  |     |
 * |-----+-----+-----+-----+-----+-----+-----+-----+-----|-----+-----+-----+-----+-----+-----|
 * |     |  A  |  S  |  D  |  F  |  G  |     |     |     |  H  |  J  |  K  |  L  |  ;  |     |
 * |-----+-----+-----+-----+-----+-----+-----+-----+-----|-----+-----+-----+-----+-----+-----|
 * |     |  Z  |  X  |  C  |  V  |  B  |     |     |     |  N  |  M  |  :  |  \  |  /  |     |
 * |-----+-----+-----+-----+-----+-----+-----+-----------+-----+-----+-----+-----+-----+-----|
 * |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |
 * `-----------------------------------------------------------------------------------------'
 */		
	[QWERTY] = LAYOUT(
		_______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______,
		_______, KC_Q,    KC_W,    KC_E,    KC_R,    KC_T,    _______, _______, _______, KC_Y,    KC_U,    KC_I,    KC_O,    KC_P,    _______,
		_______, KC_A,    KC_S,    KC_D,    KC_F,    KC_G,    _______, _______, _______, KC_H,    KC_J,    KC_K,    KC_L,    KC_SCLN, _______,
		_______, KC_Z,    KC_X,    KC_C,    KC_V,    KC_B,    _______, _______, _______, KC_N,    KC_M,    KC_COLN, KC_BSLS, KC_SLSH, _______,
		_______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______
	),
/* Keymap 2: RULEMAK
 *
 * ,-----------------------------------------------------------------------------------------.
 * |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |
 * |-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----|
 * |  Ъ  |  Я  |  В  |  Ф  |  П  |  Г  |     |     |     |  Й  |  Л  |  У  |  Ы  |  Ю  |  Ч  |
 * |-----+-----+-----+-----+-----+-----+-----+-----+-----|-----+-----+-----+-----+-----+-----|
 * |     |  А  |  Р  |  С  |  Т  |  Д  |     |     |     |  Х  |  Н  |  Е  |  И  |  О  |     |
 * |-----+-----+-----+-----+-----+-----+-----+-----+-----|-----+-----+-----+-----+-----+-----|
 * |     |  З  |  Ь  |  Ц  |  Ж  |  Б  |     |     |     |  К  |  М  |  Ш  |  Щ  |  Э  |     |
 * |-----+-----+-----+-----+-----+-----+-----+-----------+-----+-----+-----+-----+-----+-----|
 * |     |     |     |  ,  |  .  |     |     |     |     |     |     |     |     |     |     |
 * `-----------------------------------------------------------------------------------------'
 */		
	[RULEMAK] = LAYOUT(
		_______, _______, _______, _______,    _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______,
		KC_RBRC, KC_Z,    KC_D,    KC_A,       KC_G,    KC_U,    _______, _______, _______, KC_Q,    KC_K,    KC_E,    KC_S,    KC_DOT,  KC_X,
		_______, KC_F,    KC_H,    KC_C,       KC_N,    KC_L,    _______, _______, _______, KC_LBRC, KC_Y,    KC_T,    KC_B,    KC_J,    _______,
		_______, KC_P,    KC_M,    KC_W,       KC_SCLN, KC_COMM, _______, _______, _______, KC_R,    KC_V,    KC_I,    KC_O,    KC_QUOT, _______,
		_______, _______, _______, S(KC_SLSH), KC_SLSH, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______
	),
/* Keymap 3: UPPER
 *
 * ,-----------------------------------------------------------------------------------------.
 * | ver | F1  | F2  | F3  | F4  | F5  |     | APP |BLDR | F6  | F7  | F8  | F9  | F10 | F11 |
 * |-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----|
 * |QWERT|  !  |  @  |  {  |  }  |  |  |SAVE |     |  \  |  /  |  7  |  8  |  9  |  *  | F12 |
 * |-----+-----+-----+-----+-----+-----+-----+-----+-----|-----+-----+-----+-----+-----+-----|
 * |RUSLA|  #  |  $  |  (  |  )  |  `  |  ?  | PGUP|  :  |  _  |  4  |  5  |  6  |  +  | NUM |
 * |-----+-----+-----+-----+-----+-----+-----+-----+-----|-----+-----+-----+-----+-----+-----|
 * |  "  |  %  |  ^  |  [  |  ]  |  ~  |RGUI | PGDN|     |  &  |  1  |  2  |  3  |  -  | CAPS|
 * |-----+-----+-----+-----+-----+-----+-----+-----------+-----+-----+-----+-----+-----+-----|
 * |RCTL |     |     |  <  |  >  |     |     |     |     |     |     |  .  |  0  |  =  | PS  |
 * `-----------------------------------------------------------------------------------------'
 */		
	[UPPER] = LAYOUT(
		VRSN,        KC_F1,   KC_F2,   KC_F3,   KC_F4,   KC_F5,   _______, KC_APP,  QK_BOOT, KC_F6,   KC_F7,   KC_F8,  KC_F9, KC_F10,  KC_F11,
		TG(QWERTY),  KC_EXLM, KC_AT,   KC_LCBR, KC_RCBR, KC_PIPE, KC_SAV,  _______, KC_BSLS, KC_SLSH, KC_7,    KC_8,   KC_9,  KC_ASTR, KC_F12,
		TG(RULEMAK), KC_HASH, KC_DLR,  KC_LPRN, KC_RPRN, KC_GRV,  KC_QUES, KC_PGUP, KC_COLN, KC_UNDS, KC_4,    KC_5,   KC_6,  KC_PLUS, KC_NUM,
		KC_DQUO,     KC_PERC, KC_CIRC, KC_LBRC, KC_RBRC, KC_TILD, KC_RGUI, KC_PGDN, _______, KC_AMPR, KC_1,    KC_2,   KC_3,  KC_MINS, KC_CAPS,
		KC_RCTL,     _______, _______, KC_LT,   KC_GT,   _______, _______, _______, _______, _______, _______, KC_DOT, KC_0,  KC_EQL,  KC_PSCR
	),
	[VORTOJ] = LAYOUT(
		_______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______,
		_______, VORT00,  VORT00,  VORT00,  VORT00,  VORT00,  _______, _______, _______, VORT00,  VORT00,  VORT00,  VORT00,  VORT00,  _______,
		_______, VORT00,  VORT00,  VORT00,  VORT00,  VORT00,  _______, _______, _______, VORT00,  VORT00,  VORT00,  VORT00,  VORT00,  _______,
		_______, VORT00,  VORT00,  VORT00,  VORT00,  VORT00,  _______, _______, _______, VORT00,  VORT00,  VORT00,  VORT00,  VORT00,  _______,
		_______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______
	),
	[MOUSE] = LAYOUT(
		_______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______,
		_______, _______, _______, KC_MS_U, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______,
		_______, _______, KC_MS_L, KC_MS_D, KC_MS_R, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______,
		_______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, KC_MPRV, KC_MNXT, _______, _______,
		_______, _______, _______, KC_BTN1, KC_BTN2, KC_WBAK, _______, _______, _______, _______, KC_VOLU, KC_VOLD, KC_MUTE, _______, _______
	)
};


/* DISABLED
void matrix_init_user(void) {
}

void matrix_scan_user(void) {
}

*/

bool process_record_user(uint16_t keycode, keyrecord_t *record) {

  uint8_t layer = get_highest_layer(layer_state);
  uint8_t col = record->event.key.col;
  uint8_t row = record->event.key.row;

  switch (keycode) {
    case ANY_ALT:
      if (record->event.pressed) {
		if (layer == RULEMAK) {
          layer_off(RULEMAK);
          old_rlmk = 1;
        }else{
          old_rlmk = 0;
	    }
        register_code(KC_LALT);
      } else {
        unregister_code(KC_LALT);
        if (old_rlmk == 1) {
          layer_on(RULEMAK);          
	    }
      }
      return false;
    case ANY_CTRL:
      if (record->event.pressed) {
		if (layer == RULEMAK) {
          layer_off(RULEMAK);
          old_rlmk = 1;
        }else{
          old_rlmk = 0;
	    }
        register_code(KC_LCTL);
      } else {
        unregister_code(KC_LCTL);
        if (old_rlmk == 1) {
          layer_on(RULEMAK);          
	    }
      }
      return false;
  }
  if (record->event.pressed) {
    switch (keycode) {
      case VRSN:
        SEND_STRING(QMK_KEYBOARD "/" QMK_KEYMAP " @ " QMK_VERSION);
        return false;
      case RUSLAT:
        SEND_STRING(SS_LCTL(SS_TAP(X_LSFT)));
		SEND_STRING(SS_LALT(SS_TAP(X_LSFT)));
		if (layer<RULEMAK) {
		  layer_on(RULEMAK);
		}else{
		  layer_off(RULEMAK);
		}
        return false;
      case VORT00:
        do_password (col, row);
        return false;
    }
  }
  return true;
}

layer_state_t layer_state_set_user(layer_state_t state)
{
    return state;
}
