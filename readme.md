# xd75re handwired 5x15 keyboard

![xd75re](https://kprepublic.com/cdn/shop/products/HTB1pFuiSXXXXXbTXFXXq6xXFXXXQ.jpg)

This keyboard handwired as a 8x10 board, so it uses a Pro Micro as a MCU. As all of 18 easyly accesible pins used it has no leds.

Keyboard itself has a 5x15 layout, and very usefull as a ortho. It's possible to split alpha-keys to left and right part of the board with mods in between.

Keyboard Maintainer: [Mikhail Vakhrushev](https://rocketgit.com/user/armatus/xd75re_handwired)  

Hardware:
  * Handwired
  * Uses a Pro Micro

Make example for this keyboard (after setting up your build environment):

    copy contents of directory qmk_keyboard_dir into qmk_firmware/keyboards/ 
    into the qmk_firmware directory run:
    make handwired/xd75re_5x15:default

See the [build environment setup](https://docs.qmk.fm/#/getting_started_build_tools) and the [make instructions](https://docs.qmk.fm/#/getting_started_make_guide) for more information. Brand new to QMK? Start with our [Complete Newbs Guide](https://docs.qmk.fm/#/newbs).
